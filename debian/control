Source: libwacom
Section: libs
Priority: optional
Maintainer: Timo Aaltonen <tjaalton@debian.org>
Build-Depends: debhelper-compat (= 13),
 meson,
 libevdev-dev,
 libglib2.0-dev,
 libgudev-1.0-dev,
 libxml2-dev <!nocheck>,
 pkgconf,
 python3-libevdev <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pyudev <!nocheck>,
 systemd-dev,
 udev <!nocheck>,
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/debian/libwacom.git
Vcs-Browser: https://salsa.debian.org/debian/libwacom

Package: libwacom9
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 libwacom-common,
Suggests:
 libwacom-bin (= ${binary:Version}),
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: Wacom model feature query library
 libwacom is a library to identify wacom tablets and their model-specific
 features. It provides easy access to information such as "is this a built-in
 on-screen tablet", "what is the size of this model", etc.

Package: libwacom-bin
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 python3:any,
 python3-libevdev,
 python3-pyudev,
Multi-Arch: foreign
Description: Wacom model feature query library -- binaries
 libwacom is a library to identify wacom tablets and their model-specific
 features. It provides easy access to information such as "is this a built-in
 on-screen tablet", "what is the size of this model", etc.
 .
 This package contains the utilities which are used by the libraries.

Package: libwacom-common
Architecture: all
Depends: ${misc:Depends},
Multi-Arch: foreign
Description: Wacom model feature query library (common files)
 libwacom is a library to identify wacom tablets and their model-specific
 features. It provides easy access to information such as "is this a built-in
 on-screen tablet", "what is the size of this model", etc.
 .
 This package contains common data files for libwacom.

Package: libwacom-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
 libwacom9 (= ${binary:Version}),
 libevdev-dev,
 libglib2.0-dev,
 libgudev-1.0-dev,
Recommends: pkgconf
Description: Wacom model feature query library (development files)
 libwacom is a library to identify wacom tablets and their model-specific
 features. It provides easy access to information such as "is this a built-in
 on-screen tablet", "what is the size of this model", etc.
 .
 This package contains the development files for libwacom.
